import catppuccin
import os
os.environ["PATH"] = "/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/bin"

# load your autoconfig, use this, if the rest of your config is empty!
config.load_autoconfig()

# set the flavor you'd like to use
# valid options are 'mocha', 'macchiato', 'frappe', and 'latte'
# last argument (optional, default is False): enable the plain look for the menu rows
catppuccin.setup(c, 'mocha', True)

config.set("colors.webpage.darkmode.enabled", True)

config.set("fonts.default_size", "16pt")
config.set("fonts.web.size.default", 24)
config.set("fonts.web.size.default_fixed", 20)

# Aliases
c.aliases['wallabag'] = 'spawn -v -m wallabag add {url}'
c.aliases['bitwarden'] = 'spawn --userscript qute-bitwarden'
c.aliases['mpv'] = 'spawn -d mpv {url}'
c.aliases['taskreview'] = 'spawn -v -m task add Review {url}'
